<?php 
	require "../partials/template.php";

	function get_title(){
		echo "Order History";
	}

	function get_body_contents(){
?>
	<h1 class="text-center py-5">Order History</h1>
	<hr class="border-white">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Order Id</th>
				<th>Items</th>
				<th>Total</th>
			</tr>
		</thead>
		<tbody>
			<?php
				$userId = $_SESSION['user']['id'];

				$order_query = "SELECT * FROM orders WHERE user_id = $userId";

				$orders = mysqli_query($conn, $order_query);

				foreach($orders as $indiv_order){
			?>
				<tr>
					<td><?php $indiv_order['id'] ?></td>
					<td>
						<?php
							$orderId = $indiv_order['id'];

							$items_query = "SELECT * FROM items JOIN item_order ON (items.id = item_order.item_id) WHERE item_order.order_id = $orderId";

							$items = mysqli_query($conn, $items_query);

							foreach($items as $indiv_item){
						?>	
							<span><?php echo $indiv_item['name']</span><br>
						?>
						}
						?>
					</td>
					<td><?php echo $indiv_order['total']?></td>
				</tr>
			<?php
				}
			?>
		</tbody>
	</table>